package com.oreo.taste.jdk.seventeen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * JDK support roadmap
 * https://www.oracle.com/java/technologies/java-se-support-roadmap.html
 */
@SpringBootApplication
public class TasteJdkSeventeenApplication {

//    refer https://www.oracle.com/java/technologies/javase/jdk17-readme-downloads.html
// TODO TASTE
//          -XX:+UseZGC               (jdk 17)
//          -XX:+UseG1GC              (jdk 17)
//          -XX:+UseShenandoahGC      (jdk 17)
// 		RandomGeneratorFactory
//	    密封类（Sealed Class）

    public static void main(String[] args) {
        SpringApplication.run(TasteJdkSeventeenApplication.class, args);
    }


    public strictfp float add(float x1, float x2) {
        return x1 + x2;
    }


    private void test() {


        int x = 3;
        String s = switch (x) {
            case 0:
                yield "0";
            case 1: {
                if (x < 6) {
                    yield "1";
                } else {
                    yield "10";
                }
            }

            default:
                yield "err";
        };
        System.out.println(s);

        String s2 = switch (x) {
            case 0 -> "0";
            case 1 -> "1";
            case 2 -> "2";
            default -> "err";
        };
        System.out.println(s2);


        Object obj = "s";
        if (obj instanceof String str) {
            System.out.println(str);
        }
    }

}
