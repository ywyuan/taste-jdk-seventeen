package com.oreo.taste.jdk.seventeen.client;

import org.springframework.web.service.annotation.GetExchange;

import java.util.List;

/**
 * UserApiService
 *
 * @author yuan
 * @since 2023/4/2 21:53
 */
public interface UserApiService {
    @GetExchange("/users")
    List<User> getUsers();
}
