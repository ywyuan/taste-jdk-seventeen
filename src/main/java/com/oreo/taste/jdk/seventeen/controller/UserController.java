package com.oreo.taste.jdk.seventeen.controller;

import com.oreo.taste.jdk.seventeen.client.User;
import com.oreo.taste.jdk.seventeen.client.UserApiService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * UserController
 *
 * @author yuan
 * @since 2023/4/2 21:55
 */
@RestController
public class UserController {

    @GetMapping("/users")
    public List<User> list() {
        return IntStream.rangeClosed(1, 10)
                .mapToObj(i -> new User(i, "User" + i))
                .collect(Collectors.toList());
    }

    @GetMapping("/test")
    public List<User> testList() {
        WebClient client = WebClient.builder().baseUrl("http://127.0.0.1:8080").build();
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builder(WebClientAdapter.forClient(client)).build();
        // 创建 代理过程
        UserApiService service = factory.createClient(UserApiService.class);
        List<User> users = service.getUsers();
        for (User user : users) {
            System.out.println(user);
        }

        return users;
    }
}
